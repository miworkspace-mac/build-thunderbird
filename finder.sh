#!/bin/bash

NEWLOC=`curl -L https://www.mozilla.org/en-US/thunderbird/all/   2>/dev/null | /usr/local/bin/htmlq -a href a | grep lang=en-US | grep os=osx | head -1`


if [ "x${NEWLOC}" != "x" ]; then
	echo ${NEWLOC}
fi